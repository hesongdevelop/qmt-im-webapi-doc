#########
留言接口
#########

留言接口实际上不是用于即时消息功能，它不具备时效性。

全媒体提供简单的留言记录功能接口。该接口的访问方式、返回结果格式与验证方式与其它IM功能接口一致。

**************
收到客户的留言
**************

该接口由 **全媒体系统提供** 。当收到了客户的留言，APP应调用该接口告知全媒体系统。

访问地址
========
:Path: ``/api/<version>/<appid>/leavemessage``
:Method: ``POST``

请求数据
========

使用HTTP请求的消息体记录要发送的消息。采用JSON格式。其属性有：

* ``user_id``
    :数据类型: ``str``

    如果APP能够提供用户的唯一ID，就在这个字段填写。如果无法确定，则可不提供该字段。

* ``contact``
    :数据类型: ``str``

    [``必须``]

    联系人的称呼，最少2个字符，最多32个字符

* ``callback``
    :数据类型: ``str``

    [``必须``]

    回复方式

    客服人员将按照用户要求的回复方式进行回访

    支持的回复方式有：

    ============= ===================
    标识符        说明
    ============= ===================
    email         电子邮件
    fax           传真
    sms           短信
    tel           电话
    ============= ===================

* ``tel``
    :数据类型: ``str``

    电话号码

    当回复方式是电话、传真、短信时，该属性必填。

    固定电话格式是区号+电话号码,例如:07522026666。

* ``email``
    :数据类型: ``str``

    邮箱地址

    当回复方式是电子邮件时，该属性必填

* ``subject``
    :数据类型: ``str``

    [``必须``]

    留言主题

    最多50字符

* ``content``
    :数据类型: ``str``

    留言内容

    最多2000字符

* ``user_data``

    自定义数据

    APP 可在此字段填写任何 JSON 数据，作为留言的附带数据。

回复数据
========
如果返回错误，返回值参见 :doc:`api_ret_code`

举例
========

* 当用户在APP提供的表单上完成填写后，APP向全媒体系统发送POST:

    .. code-block:: http

        POST /api/v1.0/1001/leavemessage?timestamp=1407812629434&signature=xxxxxxx HTTP/1.1
        Content-Type: application/json
        Content-Length: xxx

        {
            "user_id" : "198979273423",
            "contact" : "王五",
            "callback" : "email",
            "email" : "wangwu@163.com",
            "subject" : "空调没人来装呀，你们的安装肿么了？",
            "content" : "",
            "user_data" : {
                "geolocation": [12.23, 40.1],
                "app_version": "1.2.1231",
                "os": "iOS 6.0"
            }
        }

.. note:: 根据实际情况，所需属性可能有所调整

**************
数据库表结构
**************

所有的数据都记录在数据库的表中，其表定义如下：

* 表名:
    cc_misc_lvmsg

* 字段定义:

    ================= ================ ========= ==========
    字段名            数据类型         其它      说明
    ================= ================ ========= ==========
    id                varchar(50)      pk        使用UUID作为主键
    acc_type          varchar(50)      nn        留言来源的账户类型。微信请自行填写"weixin"，否则是"app"
    acc_name          varchar(50)      nn        账户名。微信的需要自行填写微信账户名，app的是app的name
    user_id           varchar(50)                如果APP能够提供用户的唯一ID，就在这个字段填写。如果无法确定，则可不提供该字段。
    contact           varchar(32)      nn        联络人称谓
    callback          varchar(50)      nn        回复方式
    tel               varchar(45)      
    email             varchar(100)
    subject           VARCHAR(50)      nn        留言主题
    content           VARCHAR(2000)              留言内容
    create_time       timestamp        nn
    user_data         VARCHAR(2000)              附加数据
    tag               VARCHAR(45)                标签，开发可任意处置
    ================= ================ ========= ==========

* DDL:

.. code-block:: mysql

    CREATE TABLE `cc_misc_lvmsg` (
      `id` varchar(50) NOT NULL,
      `acc_type` varchar(50) NOT NULL COMMENT '留言来源的账户类型。微信请自行填写"weixin"，否则是"app"',
      `acc_name` varchar(50) NOT NULL COMMENT '账户名。微信的需要自行填写微信账户名，app的是app的name',
      `user_id` varchar(50) DEFAULT NULL COMMENT '如果APP能够提供用户的唯一ID，就在这个字段填写。如果无法确定，则可不提供该字段。',
      `contact` varchar(32) NOT NULL COMMENT '联络人称谓',
      `callback` varchar(50) NOT NULL COMMENT '回复方式',
      `tel` varchar(45) DEFAULT NULL,
      `email` varchar(100) DEFAULT NULL,
      `subject` varchar(50) NOT NULL COMMENT '留言主题',
      `content` varchar(2000) DEFAULT NULL COMMENT '留言内容',
      `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `user_data` varchar(2000) DEFAULT NULL COMMENT '附加数据',
      `tag` varchar(45) DEFAULT NULL COMMENT '标签，开发可任意处置',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='留言记录';

