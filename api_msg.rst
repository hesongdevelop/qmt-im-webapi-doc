#########
消息接口
#########

**************
收到客户的消息
**************

该接口由 **全媒体系统提供** 。当收到了客户发送的消息，APP应调用该接口告知全媒体系统。

访问地址
========
:Path: ``/api/<version>/<appid>/staffService/message``
:Method: ``POST``

Headers
========
:Content-Type: ``application/json``

.. warning:: ``Content-Type`` 头域的值必须是 ``application/json``

请求数据
========

使用HTTP请求的消息体记录要发送的消息。采用JSON格式。其属性有：

* ``FromUserId``
    :数据类型: ``str``

    [``必须``]

    发送该消息的客户的ID

* ``FromUserName``
    :数据类型: ``str``

    [``可选``]

    用户的显示名

* ``ServiceId``
    :数据类型: ``str``

    [``可选``]

    该消息所对应的人工对话的ID。正常情况下，如果客户未同时处于多个人工对话中，则可以不提供该属性，或者设置其值为 ``null`` 或者 ``""``。

* ``MsgType``
    :数据类型: ``str``

    [``必须``]
    
    消息类型。详见 :doc:`api_msg_fmt` 一节。

* ``Content``
    :数据类型: ``str``

    [``必须``]

    消息正文

    如果是文本消息，该属性值应是原始的文本数据。

    如果是图片、声音、文件等多媒体消息，该属性值应是这些多媒体文件的下载地址。

    更多信息详见 :doc:`api_msg_fmt` 一节。

* ``Tag``
    :数据类型: 任意 JSON 数据

    [``可选``]

    消息的附加数据

回复数据
========
参见 :doc:`api_ret_code`

举例
====

#. APP发起HTTP请求:

    .. code-block:: http

        POST /api/v1/1001/staffService/message?timestamp=1407812629434&signature=xxxxxxx HTTP/1.1
        Content-Type: application/json
        Content-Length: xxx

        {
            "FromUserId" : "123",
            "MsgType" : "text",
            "Content" : "Hello World!"
        }

#. 全媒体在处理完这个消息后，回复：

    .. code-block:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: xxx

        {
            "errcode" : 0,
            "errmsg" : "ok"
        }

**************
向客户发送消息
**************

该接口由 **APP 提供** 。当全媒体系统需要向客户发送消息，就向APP发送此请求。

访问地址
========
:Path: ``/<path_prefix>/staffService/message``
:Method: ``POST``

请求数据
========

使用HTTP请求的消息体记录要发送的消息。采用JSON格式。其属性有：

* ``ToUserId``
    :数据类型: ``str``

    [``必须``]

    向该拥有该ID客户发送消息

* ``WorkerNum``
    :数据类型: ``str``

    [``可选``]

    如果该消息是来自于坐席，那么这个属性记录了该坐席的工号

* ``WorkerName``
    :数据类型: ``str``

    [``可选``]

    如果该消息是来自于坐席，那么这个属性记录了该坐席的显示名


* ``ServiceId``
    :数据类型: ``str``

    [``可选``]

    如果该消息来自于一个人工服务会话，则该属性记录了这个会话的ID

* ``MsgType``
    :数据类型: ``str``

    [``必须``]
    
    消息类型。详见 :doc:`api_msg_fmt` 一节。

* ``Content``
    :数据类型: ``str``

    [``必须``]

    消息正文

    如果是文本消息，该属性值应是原始的文本数据。

    如果是图片、声音、文件等多媒体消息，该属性值应是这些多媒体文件的下载地址。

    更多信息详见 :doc:`api_msg_fmt` 一节。

* ``Tag``
    :数据类型: 任意 JSON 数据

    [``可选``]

    消息的附加数据

回复数据
========
参见 :doc:`api_ret_code`

举例
====

#. 全媒体系统发起HTTP请求:

    .. code-block:: http

        POST /qmtapi/staffService/message?timestamp=1407812629434&signature=xxxxxxx HTTP/1.1
        Content-Type: application/json
        Content-Length: xxx

        {
            "ToUserId" : "123",
            "MsgType" : "text",
            "Content" : "亲，你好，欢迎回来。"
        }

#. APP在处理完消息后回复：

    .. code-block:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: xxx
        
        {
            "errcode" : 0,
            "errmsg" : "ok"
        }
