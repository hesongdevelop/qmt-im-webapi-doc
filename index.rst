.. 全媒体系统即时消息客服对外接口 documentation master file, created by
   sphinx-quickstart on Thu Jul 24 10:17:35 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##############################
全媒体系统即时消息客服对外接口
##############################

**目录**

.. toctree::
   :maxdepth: 2

   概述 <abstract>

   API 参考 <api_index>

   安全问题 <safty>
