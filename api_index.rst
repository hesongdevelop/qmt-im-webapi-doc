########
API 参考
########

本章节定义了全媒体以及对接系统所应提供的HTTP访问接口与工作方式。

------

.. important::

    API文档中的 ``url`` 部分只定义了路径，没有定义参数。但是实际上，所有的 HTTP 请求都需要提供参数进行安全验证，具体参见 :doc:`safty` 章节。

.. attention::

    所有提交数据与返回数据都采用 ``JSON`` 格式，在API文档中多处使用表格和列表来表现 ``JSON`` 对象的键值对格式。所有的HTTP包体均采用 **UTF-8** 编码格式。

.. attention::

    在 URL 的 Path 部分，格式是 ``\api\<version>\xxxx`` 的形式，其中的版本部分用于区别对待不同版本的服务程序。本文档所对应的是版本为 ``1.0`` 的程序，该部分应该填写 ``v1.0`` 。

------

**目录**

.. toctree::
   :maxdepth: 2

   消息接口 <api_msg>

   消息格式 <api_msg_fmt>

   排队接口（不再使用） <api_que>

   留言接口 <api_lvmsg>

   返回码 <api_ret_code>


    
