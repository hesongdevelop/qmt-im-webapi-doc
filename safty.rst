########
安全问题
########

全媒体系统的IM客服对外接口通过URL参数进行安全验证

全媒体系统的安全验证
********************

当APP向全媒体系统提交HTTP请求时， **必须** 附带若干URL参数，作为验证依据。

全媒体系统在收到请求时，会进行验证，并拒未通过绝验证的请求。

参数
====

* ``timestamp``
    时间戳

    形如： ``1407812629434``    注释：Java中的获取方法 -- Date().getTime()

* ``signature``
    签名

    签名由 ``APPID`` , ``APPSECRET`` , ``timestamp`` 计算得出

算法
====

APP应将 ``signature`` 参数填写为 SHA1 哈希值的16进制字符串表达式

.. math::

    signature = sha1(appid + appsecret + timestamp)

以 Python 语言举例::

    >>> from hashlib import sha1
    >>> appid = b'1001'
    >>> appsecret = b'123456abcdef'
    >>> timestamp = b'1407812629434'
    >>> signature = sha1(appid + appsecret + timestamp).hexdigest()
    >>> print(signature)
    b1d11d44bcb28caa6ce4dc1b7f1526ede00f49e0

此时，url形如::

    http://api/v1.0/1001/staffService/message?timestamp=1407812629434&signature=b1d11d44bcb28caa6ce4dc1b7f1526ede00f49e0

APP的安全验证
*************
全媒体系统向APP发送HTTP请求是，也会在URL的参数部分附带验证数据。APP可以根据这些参数进行验证。

参数
====

* ``timestamp``
    时间戳

    形如： ``1407812629434``

* ``signature``
    签名

    签名由 ``APPID`` , ``ACCESSTOKEN`` , ``timestamp`` 计算得出

算法
====

全媒体系统将 ``signature`` 参数填写为 SHA1 哈希值的16进制字符串表达式

.. math::

    signature = sha1(appid + accesstoken + timestamp)

以 Python 语言举例::

    >>> from hashlib import sha1
    >>> appid = b'1001'
    >>> accesstoken = b'6c9hgd@%#^5evc75@53Z5'
    >>> timestamp = b'1407812629434'
    >>> signature = sha1(appid + accesstoken + timestamp).hexdigest()
    >>> print(signature)
    04326e341d0ba7064975a9c03a75361f856d3341

此时，url形如::

    http://apphost/staffService/message?timestamp=1407812629434&signature=04326e341d0ba7064975a9c03a75361f856d3341
